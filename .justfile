# list all just entires
default: 
    just --list

# generate cloudflare-api-secret
cloudflare:
    #!/usr/bin/env bash
    read -p "Enter Cloudflare API key: " CF_API_KEY && echo
    read -p "Enter Cloudflare API token: " CF_API_TOKEN && echo
    kubectl create secret generic cloudflare-api-secret \
        --from-literal=api-key="$CF_API_KEY" \
        --from-literal=api-token="$CF_API_TOKEN"

# helm update
update:
    helm dependency update

# helm upgrade
upgrade:
    helm upgrade meerkat .

#helm install
install:
    helm install meerkat .

# helm uninstall
uninstall:
    helm uninstall meerkat
